const Database = require('arangojs').Database;
const fs = require('fs');
const joi = require('joi');

let appCfg = (function() {
	let cfg = JSON.parse(fs.readFileSync('app.config.json', 'utf8'));

	let validator = joi.object().keys({
		info: joi.object().keys({
			url: joi.string(),
			port: joi.number(),
			user: joi.string(),
			pwd: joi.string(),
			name: joi.string(),
			graph: joi.string()
		}),
		schema: joi.object().pattern(joi.string(), joi.object().keys({
			from: joi.string(),
			to: joi.string()
		}))
	});

	validator.validate(cfg);
	return cfg;
})();

let schema = (function() {
	const db = createDBHandler();
	const graph = db.graph(appCfg.info.graph);

	let res = {
		edge: {},
		vertex: {},
		arango: db
	};

	for(let edgeName in appCfg.schema) {
		res.edge[edgeName] = graph.edgeCollection(edgeName);

		res.vertex[appCfg.schema[edgeName].from] = graph.vertexCollection(appCfg.schema[edgeName].from);
		res.vertex[appCfg.schema[edgeName].to] = graph.vertexCollection(appCfg.schema[edgeName].to);
	}

	return res;
})();

function createDBHandler() {
	const dbHandler = new Database({ url: `http://${appCfg.info.url}:${appCfg.info.port}` });
	dbHandler.useBasicAuth(appCfg.info.user, appCfg.info.pwd);
	dbHandler.useDatabase(appCfg.info.name);

	return dbHandler;
}

async function update() {	
	const db = createDBHandler();
	db.useDatabase('_system');
	console.log('Checking DB...');

	try {
		await db.createDatabase(appCfg.info.name);
		console.log('DB was created.');
	} catch(e) {
		console.log('DB already exists.');
	}	

	console.log('DB ready.');
	db.useDatabase(appCfg.info.name);

	console.log('Checking schema...');

	if(!await db.graph(appCfg.info.graph).exists()) {
		console.log('Graph doesn\'t exists. Creating...');
		await db.graph(appCfg.info.graph).create();
	} 
	else {
		console.log('Graph already exists.');
	}

	console.log('Graph ready.');
	console.log('Updating graph structure...');

	let graph = db.graph(appCfg.info.graph);
	let edges = await (await graph.get()).edgeDefinitions;
	let toRemove = {};
	let toReplace = {};
	let config = Object.assign({}, appCfg.schema);	

	for(let i in edges) {
		let edge = edges[i];
		let edgeName = edge.collection;

		if(config[edgeName] === undefined){
			toRemove[edgeName] = 1;
		}
		else if(edge.from.length != 1 || edge.to.length != 1 ||
			edge.from[0] !== config[edgeName].from || edge.to[0] !== config[edgeName].to) {
			toReplace[edgeName] = 1;
		}
		else {
			console.log(`Edge '${edgeName}' is up to date.`);
			delete config[edgeName];
		}
	}

	for(let rem in toRemove) {
		console.log(`Removing edge '${rem}'`);
		await graph.removeEdgeDefinition(rem, false);
	}

	for(let upd in toReplace) {
		console.log(`Updating edge '${upd}'`);

		await graph.replaceEdgeDefinition(upd, {
			collection: upd,
			from: [ config[upd].from ],
			to: [ config[upd].to ]
		});

		delete config[upd];
	}

	for(let newEdge in config) {
		console.log(`Adding new edge '${newEdge}'`);

		await graph.addEdgeDefinition({
			collection: newEdge,
			from: [ config[newEdge].from ],
			to: [ config[newEdge].to ]
		});
	}
}

async function empty() {
	for(let edgeName in schema.edge) {
		let edge = schema.edge[edgeName];
		console.log(`Emptying edge collection ${edge.name}`);
		await edge.truncate();
	}

	console.log('Edge collections truncated');

	for(let vertexName in schema.vertex) {
		let vertex = schema.vertex[vertexName];
		console.log(`Emptying vertex collection ${vertex.name}`);
		await vertex.truncate();
	}

	console.log('Vertex collections truncated');
}

module.exports = {
	schema,
	appCfg,
	update,
	empty,	
	handler: createDBHandler
};